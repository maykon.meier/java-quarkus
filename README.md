# Teste para Java Developer

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)
### Requisitos

- Quarkus (https://quarkus.io/)
- Google books (https://developers.google.com/books/)
- Banco de dados MariaDB ou MySQL
- Docker
- Gerar pacote .jar para aplicação (gradle ou maven)
- Testes unitários e de integração (comprovação de funcionamento)

### Diferencial

- Arquitetura do projeto
- 12 factor (https://12factor.net/pt_br/)

## Como participar?

1. Faça um clone do repositório.
2. Compartilhe seu código no seu Git de preferência e envie para o e-mail que está no fim.
3. Faremos nossa análise e te daremos um retorno.

## Detalhes da prova

- A prova consiste em criar uma interface API para pesquisar e salvar livros do Google Books em favoritos
- Seu projeto deve conter um arquivo README com a explicação das tecnologias utilizadas e as instruções para rodar.
- Descrever suas facilidades e dificuldades bem como a quantidade de horas de desenvolvimento.

### Funcionalidades

A App deve conter as seguintes funcionalidades:

1. Autenticação: Basic (usuário e senha)
2. Livros: /books (GET / pesquisa e listagem com paginação) - deve conter uma propriedade `favorite` exibindo se o livro é ou não favorito já
3. Favoritos: /with-stars (GET / para listar favoritos com paginação)
4. Favoritos: /books/{id}/favorite (POST / salvar favorito)
5. Favoritos: /books/{id}/favorite (DELETE / remover favorito)

### Especificações técnicas

* O App deve se comunicar com o Google Books API pelo backend Java.
* A comprovação do funcionamento da API será pelo resultados dos testes automatizados
* Criação da base de dados e seed de informações (se houver) deve ser executado automaticamente no subir do projeto

## Dúvidas? Envio da prova?
`testes@lyncas.net`

### Desde já obrigado pelo seu interesse e boa prova!